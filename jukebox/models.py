import os.path

from django.db import models
from jukeauth.models import AuthUser


class UserFilterManager(models.Manager):
    user_id = None

    def get_queryset(self):
        """user_id is set in auth/views.py login + djangomusic.middleware"""
        return super(UserFilterManager, self).get_queryset().filter(owner=self.user_id)


class JBBaseModel(models.Model):
    objects = UserFilterManager()

    class Meta:
        abstract = True


class Playlist(JBBaseModel):
    playlistname = models.CharField(max_length=128)
    created = models.DateTimeField(auto_now_add=True, db_index=True)
    modified = models.DateTimeField(auto_now=True, db_index=True)
    owner = models.ForeignKey(AuthUser)

    class Meta:
        ordering = ["-created"]


#
# ScannedItem
#
class NotScannedItemsManager(models.Manager):
    def get_queryset(self):
        return super(NotScannedItemsManager, self).get_queryset().filter(scanflag=False)


class ScannedItemsManager(models.Manager):
    def get_queryset(self):
        return super(ScannedItemsManager, self).get_queryset().filter(scanflag=True)


class DistinctArtistsManager(models.Manager):
    def get_queryset(self):
        queryset = super(DistinctArtistsManager, self).get_queryset()
        all_artists = queryset.values_list('artist', flat=True)

        unique_artists = list(set(all_artists))
        unique_artists.sort()

        return unique_artists


class ScannedItem(models.Model):
    uid = models.CharField(max_length=32, db_index=True)
    filename = models.CharField(max_length=512)
    basename = models.CharField(max_length=512)
    dirname = models.CharField(max_length=2048)
    title = models.CharField(max_length=512)
    artist = models.CharField(max_length=256, db_index=True)
    album = models.CharField(max_length=256)
    year = models.SmallIntegerField(null=True)
    genre = models.CharField(max_length=64,null=True)
    tracknumber = models.PositiveSmallIntegerField(null=True)
    hashcontent = models.CharField(max_length=32, db_index=True)
    hashalbum = models.CharField(max_length=32, db_index=True)
    scanflag = models.BooleanField(default=True, db_index=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    playlists = models.ManyToManyField(Playlist)

    objects = models.Manager()
    not_scanned_objects = NotScannedItemsManager()
    scanned_objects = ScannedItemsManager()
    distinct_artists_objects = DistinctArtistsManager()

    def get_item_fullpath(self):
        return os.path.join(self.dirname, self.basename, self.filename)

    def get_content_type(self):
        """based on the file extension"""
        extension = os.path.splitext(self.filename)[1].lower()

        if extension == '.mp3':
            content_type = 'audio/mpeg'
        elif extension == '.flac':
            content_type = 'audio/flac'
        else:
            content_type = ''

        return content_type

    def __str__(self):
        return "%s - %s.mp3" % (self.artist, self.title)

    class Meta:
        ordering = ["-created"]



