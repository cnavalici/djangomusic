from __future__ import absolute_import
from django.db.backends.signals import connection_created


def activate_foreign_keys(sender, connection, **kwargs):
    """Enable integrity constraint with sqlite."""
    if connection.vendor == 'sqlite':
        cursor = connection.cursor()
        cursor.execute('PRAGMA foreign_keys = ON;')


def synchronous_off(sender, connection, **kwargs):
    if connection.vendor == 'sqlite':
        cursor = connection.cursor()

        # This is risky in case your machine crashes, but comes at such a performance boost
        # that we do it anyway.
        cursor.execute('PRAGMA synchronous=OFF;')


connection_created.connect(activate_foreign_keys)
connection_created.connect(synchronous_off)
