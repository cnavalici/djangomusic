# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url, include
from .views import dashboard, playlist, player

urlcollection = ([
    url(r'^update$', dashboard.update_collection, name='update_collection'),
    url(r'^update/(?P<dummy_flag>dummy)$', dashboard.update_collection, name='update_collection'),
    url(r'^(?P<raw_element>[\w|\s]+)$', dashboard.collection, name='artists_tree_albums'),
    url(r'^$', dashboard.collection, name='artists_tree_root'),
], 'collection')

urlplaylist = ([
    url(r'^(?P<playlist_id>main|[\d]+)/$', playlist.get_playlist, name='get_playlist'),
    url(r'^create/$', playlist.create_playlist, name='create_playlist'),
    url(r'^(?P<playlist_id>main|[\d]+)/add/$', playlist.add_item_playlist, name='add_item_playlist'),
], 'playlist')

urlplayer = ([
    url(r'^play/(?P<track_id>.{36})/$', player.play_track, name="play_track")
], 'player')

urlpatterns = [
    url(r'^collection/', include(urlcollection)),
    url(r'^player/', include(urlplayer)),
    url(r'^playlist/', include(urlplaylist)),
    url(r'^$', dashboard.index, name='dashboard')
]