"""
Usage:
    python manage.py insertscanneditems
"""

import hashlib
import os.path
import random
from django.core.management.base import BaseCommand, CommandError

from jukebox.models import ScannedItem

FIXTURE_VERBS = 'verbs.txt'
FIXTURE_ARTISTS = 'artists.txt'
FIXTURE_WORDS = 'words.txt'
FIXTURE_CONJUNCTIONS = 'conjunctions.txt'

MIN_NUMBER_TRACKS_PER_ALBUM = 5
MAX_NUMBER_TRACKS_PER_ALBUM = 14

MUSIC_COLLECTION_ROOT = '/mnt/MusicCollection'


class Command(BaseCommand):
    help = 'Inserts a specified number of scanned items into db (random generated data)'

    def __init__(self):
        fixtures_path = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir, 'fixtures'))

        with open(os.path.join(fixtures_path, FIXTURE_ARTISTS)) as f:
            self.artists = f.read().splitlines()

        with open(os.path.join(fixtures_path, FIXTURE_WORDS)) as f:
            self.words = f.read().splitlines()

        with open(os.path.join(fixtures_path, FIXTURE_VERBS)) as f:
            self.verbs = f.read().splitlines()

        with open(os.path.join(fixtures_path, FIXTURE_CONJUNCTIONS)) as f:
            self.conjunctions = f.read().splitlines()

        super(Command, self).__init__()

    def add_arguments(self, parser):
        parser.add_argument('howmany', nargs='+', type=int)
        parser.add_argument('--save-to-db', action='store_true', default=False, dest="savetodb",
                            help="Commit also to db (default:False).")

    def handle(self, *args, **options):
        self.save_to_db = options['savetodb']
        howmany = options['howmany'][0]

        if howmany < MIN_NUMBER_TRACKS_PER_ALBUM:
            raise CommandError('howmany argument should be at least equal with %s' % MIN_NUMBER_TRACKS_PER_ALBUM)

        self.__build_dummy_collection(howmany)

        self.stdout.write(self.style.SUCCESS('Successfully generated %s scanned items' % howmany))

    def __build_dummy_collection(self, howmany):
        """
        generates completely dummy data starting with tracks, albums, artists
        There are coming from the fixtures and used in a random way.
        """
        while howmany > 0:
            # chose a random number of tracks and create an album from it
            many = random.randint(MIN_NUMBER_TRACKS_PER_ALBUM, MAX_NUMBER_TRACKS_PER_ALBUM)
            if howmany - many < 0:
                many = howmany

            self.__generate_album_folder(many)

            howmany -= many

    def __generate_album_folder(self, howmany_tracks):
        """
        Format: Artist - Album (year)
        similar to a folder within the collection
        :returns { 'name': 'Artists...Album', 'tracks': [<list of tracks>]}
        """
        artist = random.choice(self.artists)
        album = self.__generate_name()
        year = random.randint(1990, 2016)

        artist_album = "%s%s" % (artist, album)
        album_full = "%s - %s (%d)" % (artist, album, year)

        for number in range(1, howmany_tracks + 1):
            title = self.__generate_name()
            filename = "%02d - %s.mp3" % (number, title)
            fullpath = os.path.join(MUSIC_COLLECTION_ROOT, album_full, filename)
            album_artist_title_year = "%s%s%s%s" % (album, artist, title, year)

            si = ScannedItem()
            si.filename = filename
            si.basename = album_full
            si.dirname = MUSIC_COLLECTION_ROOT
            si.artist = artist
            si.tracknumber = number
            si.scanflag = True
            si.title = title
            si.album = album
            si.hashalbum = hashlib.md5(artist_album.encode('utf-8')).hexdigest()
            si.year = year
            si.genre = 'Rock'
            si.uid = hashlib.md5(fullpath.encode('utf-8')).hexdigest()
            si.hashcontent = hashlib.md5(album_artist_title_year.encode('utf-8')).hexdigest()

            print(si)

            if self.save_to_db:
                si.save()
                self.stdout.write(self.style.SUCCESS('Saved to db %s / %s' % (album_full, title)))


    def __generate_name(self):
        """
        Generates a generic random name
        <random_word><random_conjunction><random_word><random_verb> (1)
        OR
        <random_word><random_verb> (2)
        OR
        <random_word><random_conjunction><random_word> (3)
        """
        word_1 = random.choice(self.words)
        word_2 = random.choice(self.words)
        conj = random.choice(self.conjunctions)
        verb = random.choice(self.verbs)

        choice = random.randint(1, 3)
        if choice == 1:
            name = "%s %s %s %s" % (word_1, conj, word_2, verb)
        if choice == 2:
            name = "%s %s" % (word_2, verb)
        if choice == 3:
            name = "%s %s %s" % (word_1, conj, word_2)

        return name.capitalize()







    def __generate_fullpath(self):
        """
        starting point: generates a complete (and fake) full path
        it starts always with /mnt/music_collection/
        """
        fullpath = '/mnt/music_collection'

    def __generate_uid(self, fullpath):
        return hashlib.md5(fullpath.encode('utf-8')).hexdigest()

    # uid = models.CharField(max_length=32, db_index=True)
    # filename = models.CharField(max_length=512)
    # basename = models.CharField(max_length=512)
    # dirname = models.CharField(max_length=2048)
    # title = models.CharField(max_length=512)
    # artist = models.CharField(max_length=256, db_index=True)
    # album = models.CharField(max_length=256)
    # year = models.SmallIntegerField(null=True)
    # genre = models.CharField(max_length=64,null=True)
    # tracknumber = models.PositiveSmallIntegerField(null=True)
    # hashcontent = models.CharField(max_length=32, db_index=True)
    # hashalbum = models.CharField(max_length=32, db_index=True)
    # scanflag = models.BooleanField(default=True, db_index=True)
    # created = models.DateTimeField(auto_now_add=True)
    # modified = models.DateTimeField(auto_now=True)