from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.utils.translation import ugettext as _

from lib.helpers import parse_http_range_header
from lib.playlistmanager import PlaylistManager
from lib.userplaylistmanager import UserPlaylistManager

from jukebox.forms import PlaylistForm
from jukebox.views.mappers import playlist_map


@csrf_exempt
@require_http_methods(["GET"])
def get_playlist(request, playlist_id):
    """
    Get the playlist content using identifiers (playlist_id)
    'main' is a special identifier for the main playlist associated with an user
    """
    (start, no) = parse_http_range_header(request.META)

    ######################
    # If the byte-range-set is unsatisfiable, the server SHOULD return a response with a status of
    # 416 (Requested range not satisfiable).
    # Otherwise, the server SHOULD return a response with a status of 206 (Partial Content)
    # containing the satisfiable ranges of the entity-body.
    ######################

    upm = UserPlaylistManager(request.user)
    playlist = upm.get_playlist(playlist_id)
    # import pdb;pdb.set_trace()
    total = playlist.scanneditem_set.count()
    # records = playlist.scanneditem_set.filter(pk__gt=start, pk__lt=start+no).order_by('pk')
    records = playlist.scanneditem_set.all().order_by('pk')[start:start+no]
    records_mapped = playlist_map(records)

    response = JsonResponse(records_mapped, safe=False)
    response['Content-Range'] = ('items %s-%s/%s' % (start, start + no, total))

    return response


@csrf_exempt
@require_http_methods(["POST"])
def create_playlist(request):
    # import pdb; pdb.set_trace() ## debug only
    response = JsonResponse([], safe=False)

    try:
        pf = PlaylistForm(request.POST)

        if pf.is_valid():
            new_playlist = pf.save()
        else:
            raise Exception("Playlist couldn't be created. Missing playlist name perhaps?")

        if not request.user.main_playlist:
            request.user.main_playlist = new_playlist
            request.user.save()

        rdata = {'success': _('Playlist was created.'), 'id': new_playlist.id}

        response = JsonResponse(rdata, safe=False)
        response.status_code = 200

    except Exception as e:
        response = JsonResponse({'error': e.__str__()})
        response.status_code = 400  # HttpResponseBadRequest
    finally:
        return response


@csrf_exempt
@require_http_methods(["POST"])
def add_item_playlist(request, playlist_id):
    response = JsonResponse([], safe=False)

    try:
        if playlist_id == 'main':
            playlist = request.user.main_playlist
        else:
            playlist = Playlist.objects.get(pk=playlist_id)

        if not playlist:
            raise Exception("Playlist with id: %s couldn't be found." % (playlist_id, ))

        pm = PlaylistManager(playlist=playlist)
        pm.add_item(request.POST['item'])

        # print(request.POST, playlist, playlist_id)

        response = JsonResponse({'msg': _('Item was added to the playlist.')}, safe=False)
    except Exception as e:
        response = JsonResponse({'msg': e.message})
        response.status_code = 400  # HttpResponseBadRequest
    finally:
        return response
