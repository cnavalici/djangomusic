# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings

from django.http import HttpResponse, JsonResponse
from django.db.models import Count

from django.views.decorators.csrf import ensure_csrf_cookie

from jukebox.models import ScannedItem as ScannedItemModel
from jukebox.views.mappers import artists_map, albums_map, tracks_map

from lib.identitymanager import IdentityManager
from lib.scanner import Scanner
from lib.dbmanager import DBManager


@ensure_csrf_cookie
def index(request):
    template_data = {}

    return render_to_response('jukebox/dashboard.html',
                              template_data,
                              context_instance=RequestContext(request))


def collection(request, raw_element = None):
    response = {}

    ident = IdentityManager()
    identified = ident.run_identification(raw_element)

    if raw_element == 'root' or not identified:
        distinct_artists = ScannedItemModel.distinct_artists_objects.all()
        response = artists_map(distinct_artists)

    if ident.is_artist:
        albums_artist = ScannedItemModel.objects \
            .filter(artist=ident.content) \
            .values('hashalbum', 'album').order_by() \
            .annotate(album_tracks_count=Count('uid'))
        response = albums_map(albums_artist, ident.content)

    if ident.is_album:
        tracks_album = ScannedItemModel.objects \
            .filter(hashalbum=ident.content).values('uid', 'title') \
            .order_by('title')
        response = tracks_map(tracks_album, ident.content)

    return JsonResponse(response)


def update_collection(request, dummy_flag = False):
    """
    special crafted action to update the collection
    in the future to be embeded properly on a administration page, but for now
    a simple user action will just work
    """
    scanner = Scanner()
    scanner.set_target_path(settings.APP_MUSIC_SOURCES)
    scanner.set_force_update(True)

    if not dummy_flag:
        dbmanager = DBManager()
        dbmanager.pre_scan_preparations()
        scanner.set_callback(dbmanager.update_collection)

    scanner.scan()

    if dummy_flag:
        a = [ item.__repr__() for item in scanner.scanned_collection ]
        html = "<html><body>%s</body></html>" % ("<br>".join(a))
    else:
        dbmanager.post_scan_preparations()
        html = '<html><body>Scanned %s records and saved into db.</body></html>' % len(scanner.scanned_collection)

    return HttpResponse(html)