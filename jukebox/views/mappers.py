# -*- coding: utf-8 -*-

from urllib.parse import quote
from lib.identitymanager import IdentityManager


def artists_map(input_data, root_name = 'root'):
    top_level_artists = []
    identity = IdentityManager()

    for artist in input_data:
        top_level_artists.append({
            "name": artist,
            "id": quote(identity.identifier_artist + artist),
            "children": True
        })

    return {"id": root_name, "children": top_level_artists}


def albums_map(input_data, root_name):
    albums = []
    identity = IdentityManager()

    for album in input_data:
        album_name = album['album'] if album['album'] else '-'
        albums.append({
            "id": quote(identity.identifier_album + album['hashalbum']),
            "name": "%s (%02d)" % (album_name, album['album_tracks_count']),
            "children": True
        })

    return {"id": identity.identifier_artist + root_name, "children": albums}


def tracks_map(input_data, root_name):
    tracks = []
    identity = IdentityManager()

    for track in input_data:
        tracks.append({
            "id": quote(identity.identifier_track + track['uid']),
            "name": track['title']
        })

    return {"id": identity.identifier_album + root_name, "children": tracks}


def playlist_map(input_data):
    records = []

    for record in input_data:
        records.append({
            'id': record.pk, 'title': record.title, 'album': record.album, 'year': record.year, 'genre': record.genre,
            'uid': record.uid, 'artist': record.artist
        })

    return records