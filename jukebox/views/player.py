# -*- coding: utf-8 -*-

from django.http import HttpResponse, JsonResponse
from django.utils.translation import ugettext as _

from jukebox.models import ScannedItem

from lib.identitymanager import IdentityManager


def play_track(request, track_id):
    im = IdentityManager()
    identified = im.run_identification(track_id)

    if not identified:
        return HttpResponse(status=400, content="This doesn't look like a track to us.")

    mediafile = ScannedItem.objects.filter(uid=im.content).first()

    if not mediafile:
        return HttpResponse(content=_("Track not found."))

    print(mediafile.get_item_fullpath())

    fsock = open(mediafile.get_item_fullpath(), 'rb')

    response = HttpResponse(fsock,  content_type=mediafile.get_content_type())
    response['Content-Disposition'] = "attachment; filename=" + mediafile.__str__()
    return response
