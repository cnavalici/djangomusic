# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0006_auto_20141007_1940'),
    ]

    operations = [
        migrations.AddField(
            model_name='scanneditem',
            name='scanflag',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
