# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0012_auto_20150601_1414'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playlist',
            name='created',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='playlist',
            name='modified',
            field=models.DateTimeField(auto_now=True, db_index=True),
        ),
    ]
