# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0013_auto_20150602_1345'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playlist',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='playlist',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
