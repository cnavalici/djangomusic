# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukeauth', '0002_authuser_main_playlist'),
        ('jukebox', '0014_auto_20150602_1347'),
    ]

    operations = [
        migrations.AddField(
            model_name='playlist',
            name='owner',
            field=models.ForeignKey(default=1, to='jukeauth.AuthUser'),
            preserve_default=False,
        ),
    ]
