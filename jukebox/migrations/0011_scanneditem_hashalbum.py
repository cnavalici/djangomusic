# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0010_auto_20141010_1034'),
    ]

    operations = [
        migrations.AddField(
            model_name='scanneditem',
            name='hashalbum',
            field=models.CharField(default='', max_length=32, db_index=True),
            preserve_default=False,
        ),
    ]
