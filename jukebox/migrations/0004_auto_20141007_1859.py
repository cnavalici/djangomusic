# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0003_scanneditem_hashcontent'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='scanneditem',
            unique_together=None,
        ),
    ]
