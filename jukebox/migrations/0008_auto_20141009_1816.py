# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0007_scanneditem_scanflag'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scanneditem',
            name='scanflag',
            field=models.BooleanField(default=True),
        ),
    ]
