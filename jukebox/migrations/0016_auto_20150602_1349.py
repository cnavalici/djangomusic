# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0015_playlist_owner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playlist',
            name='created',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='playlist',
            name='modified',
            field=models.DateTimeField(auto_now=True, db_index=True),
        ),
    ]
