# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0009_auto_20141009_1822'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scanneditem',
            name='artist',
            field=models.CharField(max_length=256, db_index=True),
        ),
    ]
