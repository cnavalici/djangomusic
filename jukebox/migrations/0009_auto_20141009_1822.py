# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0008_auto_20141009_1816'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scanneditem',
            name='scanflag',
            field=models.BooleanField(default=True, db_index=True),
        ),
    ]
