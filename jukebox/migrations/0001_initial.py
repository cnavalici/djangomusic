# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ScannedItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('filename', models.CharField(max_length=512)),
                ('basename', models.CharField(max_length=512)),
                ('dirname', models.CharField(max_length=2048)),
                ('title', models.CharField(max_length=512)),
                ('artist', models.CharField(max_length=256)),
                ('album', models.CharField(max_length=256)),
                ('year', models.SmallIntegerField()),
                ('genre', models.CharField(max_length=64)),
                ('tracknumber', models.PositiveSmallIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-created'],
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='scanneditem',
            unique_together=set([('dirname', 'basename', 'filename')]),
        ),
    ]
