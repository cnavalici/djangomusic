# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0005_auto_20141007_1910'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scanneditem',
            name='hashcontent',
            field=models.CharField(max_length=32, db_index=True),
        ),
        migrations.AlterField(
            model_name='scanneditem',
            name='uid',
            field=models.CharField(max_length=32, db_index=True),
        ),
    ]
