# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0004_auto_20141007_1859'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scanneditem',
            name='genre',
            field=models.CharField(max_length=64, null=True),
        ),
        migrations.AlterField(
            model_name='scanneditem',
            name='tracknumber',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='scanneditem',
            name='year',
            field=models.SmallIntegerField(null=True),
        ),
    ]
