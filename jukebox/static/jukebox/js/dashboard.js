require(
[
    "jukebox/js/dashboard/jukebox-tree",
    "jukebox/js/dashboard/player",
    "jukebox/js/dashboard/main-playlist",
    "dojo/ready"
],
function(JukeBoxTree, Player, MainPlaylist, ready)
{
    ready(function() {
        JukeBoxTree.init();
        MainPlaylist.init();
        Player.init();
    });
});
