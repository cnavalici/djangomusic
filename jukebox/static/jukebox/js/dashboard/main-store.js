define([
    "dstore/Rest",
    "dstore/Memory",
    "dojo/_base/declare",
    "js/urls"
], function(Rest, Memory, declare, urls) {
    return {
        main_store: null,

        build_store: function() {
            //var mixin = declare([Memory, Rest]);
            this.main_store = new Rest({ target: urls.jukebox_main_playlist, useRangeHeaders: true });
            return this.main_store;
        }
    };
});