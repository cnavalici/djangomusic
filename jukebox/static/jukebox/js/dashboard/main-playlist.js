define([
    "dojo/_base/declare",
    "dstore/Rest",
    "dstore/Memory",
    "dstore/Trackable",
    "dgrid/OnDemandGrid",
    "dgrid/Selection",
    "dgrid/extensions/ColumnResizer",
    "dgrid/extensions/DnD",
    "dgrid/extensions/ColumnHider",
    "dgrid/extensions/DijitRegistry",
    "./main-store",
    "dojo/i18n!i18n/nls/jukebox",
], function(
    declare, Rest, Memory, Trackable, OnDemandGrid, Selection, ColumnResizer, DnD, ColumnHider, DijitRegistry,
    MainStore, t
){
    return {
        playlist_grid: null,

        init: function() {
            this.build_grid();
        },

        build_grid: function() {
            var grid_structure = {
                id: { label: 'id', hidden: true, unhidable: true },
                uid: { label: 'uid', hidden: true, unhidable: true },
                artist: t.grd_track_artist,
                album: t.grd_track_album,
                title: t.grd_track_title,
                year: t.grd_track_year,
                genre: t.grd_genre
            };

            var playlist_grid = new (declare([OnDemandGrid, DnD, ColumnHider, ColumnResizer]))({
                collection: MainStore.build_store(),
                columns: grid_structure,
                skin: 'claro',
                noDataMessage: t.no_results_found,
                loadingMessage: t.loading_data,
                dndParams: {
                    isSource: false,
                    allowNested: true,
                    accept: ["treeNode"],
                    checkAcceptance: function(source, nodes) {
                        return source !== this; // Don't self-accept.
                    }
                }
            }, 'main_playlist_grid');

            this.playlist_grid = playlist_grid;
            // playlist_grid.startup(); // probably not needed
        }
    };
});