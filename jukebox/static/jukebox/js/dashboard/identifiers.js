define(function() {
    return {
        identifier_artist: "ART_",
        identifier_album: "ALB_",
        identifier_track: "TRK_",

        is_artist: function(id) {
            return this.is_element(id, this.identifier_artist);
        },

        is_album: function(id) {
            return this.is_element(id, this.identifier_album);
        },

        is_track: function(id) {
            return this.is_element(id, this.identifier_track);
        },

        is_element: function(id, identifier) {
            return (id.lastIndexOf(identifier, 0) === 0);
        }
    }
});
