define([
    "dojo/on",
    "dojo/aspect",
    "dojo/request",
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/store/JsonRest",
    "dijit/Tree",
    "dijit/tree/dndSource",
    "dijit/tree/ObjectStoreModel",
    "dijit/Menu",
    "dijit/MenuItem",
    "dojo/i18n!i18n/nls/jukebox",
    "js/urls",
    "./identifiers",
    "./main-store",
    "./main-playlist"
],
function(on, aspect, request, declare, array, JsonRest, Tree, dndSource, ObjectStoreModel, Menu, MenuItem, t,
        urls, Identifier, MainStore, MainPlaylist) {
    return {
        init: function() {
            var artists_store = new JsonRest({
                target: urls.jukebox_collection,

                getChildren: function(object){
                    return this.get(object.id).then(function(fullObject){
                        return fullObject.children;
                    });
                }
            });

            // create model to interface Tree to store
            var artists_model = new ObjectStoreModel({
                store: artists_store,
         
                getRoot: function(onItem){
                    this.store.get("root").then(onItem);
                },
         
                mayHaveChildren: function(object){
                    return "children" in object;
                },
            });

             var artists_tree = new Tree({
                model: artists_model,
                showRoot: false,
                openOnClick: true,
                onDblClick: function(item, node, evt) {
                    console.log(item.id, Identifier.is_artist(item.id));
                    MainStore.main_store.add(item);
                },
                dndController: dndSource,
                copyOnly: true,
                checkAcceptance: function(source, nodes) {
                    return source !== this; // Don't self-accept.
                },
                onDndDrop: function(source, nodes, copy) {
                    console.log("onDndDrop" + source, nodes, copy);
                }
            }, "artists_tree");

            var store = MainStore.main_store;

            var menu = Menu({targetNodeIds: [artists_tree.id]});
            menu.addChild(new MenuItem({
                label: "Simple menu item"
            }));

            aspect.after(menu, "_openMyself", function(deferred, args) {
                var tn = dijit.getEnclosingWidget(deferred.target);

                array.forEach(menu.getChildren(), function(child) {
                    menu.removeChild(child);

                    menu.addChild(new MenuItem({
                        label: t.add_to_playlist.replace('%1', artists_tree.getLabel(tn.item)),
                        onClick: function() {
                            console.debug(tn.item.id);
                            request.post(urls.jukebox_add_to_playlist, {
                                data: {item: tn.item.id}
                            }).then(
                                function(response) {
                                    var r = JSON.parse(response);
                                    console.log(r.msg);
                                    console.log(MainPlaylist.playlist_grid);
                                    MainPlaylist.playlist_grid.refresh();
                                }
                            );
                        }
                    }));
                });

                return true;
            }, true);

            //artists_tree.startup();
        }
    }
});
