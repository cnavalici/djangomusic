define(
function() {
    return {
        init: function() {
            jQuery(document).ready(function($) {
                $("#jquery_jplayer_1").jPlayer({
                    ready: function () {
//                      $(this).jPlayer("setMedia", {
//                        title: "Bubble",
//                        mp3: "http://localhost:8000/jukebox/player/play/TRK_f46a183f60954792ea12ee9349ea9f4b/",
//                      });
                    },
                    cssSelectorAncestor: "#jp_container_1",
                    swfPath: "/js/jplayer",
                    supplied: "mp3",
                    solution:"html,aurora,flash",
                    auroraFormats:"flac",
                    useStateClassSkin: true,
                    autoBlur: false,
                    smoothPlayBar: true,
                    keyEnabled: true,
                    remainingDuration: true,
                    toggleDuration: true
                });

                $("#test-button").click(function() {
                    $("#jquery_jplayer_1").jPlayer("setMedia", {
                        title: "Something in the hand",
                        mp3: "http://localhost:8000/jukebox/player/play/TRK_f46a183f60954792ea12ee9349ea9f4b/",
                    });
                });
            });
        }
    }
});