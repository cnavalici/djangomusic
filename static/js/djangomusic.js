require([
    "dojo/request/notify",
    "dojo/cookie",
], function(notify, Cookie) {
    notify("send", function(response, cancel){
        response.xhr.setRequestHeader('X-CSRFToken', Cookie("csrftoken"));
    });
});