define({
    grd_loading_data: "Încărcare date",
    grd_track_title: "Titlu",
    grd_track_album: "Album",
    grd_track_artist: "Artist",
    grd_track_year: "An",
    grd_genre: "Gen",
    loading_data: "Încarcare date...",
    no_results_found: "Nici un rezultat găsit.",
    add_to_playlist: "Adaugă %1 la playlist"
});