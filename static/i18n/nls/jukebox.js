define({
    root: {
        grd_loading_data: "Loading data",
        grd_track_title: "Title",
        grd_track_album: "Album",
        grd_track_artist: "Artist",
        grd_track_year: "Year",
        grd_genre: "Genre",
        loading_data: "Loading data...",
        no_results_found: "No result found.",
        add_to_playlist: "Add %1 to playlist"
    },

    'ro': true
});