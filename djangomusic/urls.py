from django.conf.urls import patterns, include, url
from django.http import HttpResponseRedirect

from jukeauth import urls as jukeauth_urls
from jukebox import urls as jukebox_urls

urlpatterns = [
    url(r'^$', lambda x: HttpResponseRedirect('auth/')),
    url(r'^jukebox/', include(jukebox_urls.urlpatterns, namespace="jukebox")),
    url(r'^auth/', include(jukeauth_urls.urlpatterns, namespace="jukeauth")),
]
