"""
    python manage.py runserver --settings=djangomusic.conf.settings_production

    export DJANGO_SETTINGS_MODULE=djangomusic.conf.settings_production
"""

from .settings import *

APP_MUSIC_SOURCES = [
    '/home/cristian/Music/1testdata/',
    '/home/cristian/Music/2testdata/',
]

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'production.sqlite3'),
    }
}

INSTALLED_APPS += [
    'debug_toolbar.apps.DebugToolbarConfig'
]

DEBUG = True
SECRET_KEY = '#znn$vl+tq@^pu+lpc)s84073qv$^ka_gjlz@td6h(a0df3x@^'
