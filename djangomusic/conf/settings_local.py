"""
    python manage.py runserver --settings=djangomusic.conf.settings_local

    export DJANGO_SETTINGS_MODULE=djangomusic.conf.settings_local
"""

from .settings import *

APP_MUSIC_SOURCES = [
    os.path.join(BASE_DIR, '../music-data/'),
]

# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'local.sqlite3'),
    }
}

INSTALLED_APPS += [
    # 'debug_toolbar.apps.DebugToolbarConfig'
]

DEBUG = True
SECRET_KEY = '#znn$vl+tq@^pu+lpc)s84(*)qv$^ka_gjlz@td6h(a0df3x@^'
