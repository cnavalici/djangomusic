# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jukebox', '0012_auto_20150601_1414'),
        ('jukeauth', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='authuser',
            name='main_playlist',
            field=models.ForeignKey(to='jukebox.Playlist', null=True),
            preserve_default=True,
        ),
    ]
