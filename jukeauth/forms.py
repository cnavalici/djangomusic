# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext as _
from django.contrib.auth import authenticate


class AuthTokenLoginForm(AuthenticationForm):
    username = forms.CharField(
        label=_("Username"), required=True, max_length=30,
        widget=forms.TextInput(attrs={
            'trim': 'trim',
            'required': 'required',
            'data-dojo-type': 'dijit/form/ValidationTextBox',
            'data-dojo-props': "pattern:'[\\\w]+'",
            'missingMessage': _("You have to provide an username"),
            'invalidMessage': _('Invalid value')
        })
    )

    password = forms.CharField(
        label=_("Password"), required=True,
        widget=forms.PasswordInput(attrs={
            'trim': 'trim',
            'required': 'required',
            'data-dojo-type': 'dijit/form/ValidationTextBox',
            'missingMessage': _("Password is required"),
            'invalidMessage': _("Invalid value")
        })
    )

    otp_token = forms.IntegerField(
        label=_("OTPToken"), max_value=999999, required=False,
        widget=forms.TextInput(attrs={
            'trim': 'trim',
            'size': 6,
            'data-dojo-type': 'dijit/form/ValidationTextBox',
            'data-dojo-props': "pattern:'[0-9]{6}'",
            'invalidMessage': _('Invalid value, it requires 6 digits')
        })
    )

    def clean(self):
        """
        we overwrite clean method of the original AuthenticationForm to include
        the third parameter, called otp_token
        """
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        otp_token = self.cleaned_data.get('otp_token')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password,
                                           otp_token=otp_token)
            if self.user_cache is None:
                raise forms.ValidationError(self.error_messages['invalid_login'])
            elif not self.user_cache.is_active:
                raise forms.ValidationError(self.error_messages['inactive'])
        # self.check_for_test_cookie()

        return self.cleaned_data
