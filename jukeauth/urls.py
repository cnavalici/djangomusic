# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from django.http import HttpResponseRedirect
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^success/$', lambda x: HttpResponseRedirect('/jukebox'), name='success'),
]

urlpatterns += [
    url(r'^password/reset/(?P<uidb36>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm,
        {
            'template_name': 'jukeauth/password_reset_confirm.html',
            'post_reset_redirect': '/auth/password/reset/complete/'
        },
        name='password_reset_confirm'),

    url(r'^password/reset/complete/$',
        auth_views.password_reset_complete,
        {
            'template_name': 'jukeauth/password_reset_complete.html'
        },
        name='password_reset_complete'),
    url(r'^$', lambda x: HttpResponseRedirect('login/')),
]
