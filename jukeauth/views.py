# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext

from django.contrib.auth import logout as django_logout
from django.contrib.auth import login as django_login
from django.contrib.auth.views import login as django_login_view
from django.contrib.auth import authenticate

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from jukeauth.forms import AuthTokenLoginForm
from jukebox.models import UserFilterManager

def login(request):
    if request.method == 'POST':
        login_form = AuthTokenLoginForm(data=request.POST)

        if login_form.is_valid():
            username = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']
            otp_token = login_form.cleaned_data['otp_token']

            authenticated_user = authenticate(username=username, password=password, otp_token=otp_token)

            if authenticated_user:
                django_login(request, authenticated_user)
                UserFilterManager.user_id = authenticated_user.id
                return HttpResponseRedirect(reverse('jukeauth:success'))

    return django_login_view(request, template_name='jukeauth/login.html', authentication_form=AuthTokenLoginForm)


def logout(request):
    django_logout(request)
    return HttpResponseRedirect(reverse('jukeauth:login'))


def success(request):
    return render_to_response('jukeauth/success.html', {}, context_instance=RequestContext(request))
