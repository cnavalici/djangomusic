# -*- coding: utf-8 -*-

from jukeauth.models import AuthUser
from pyotp import TOTP


class AuthTokenBackend(object):
    def authenticate(self, username=None, password=None, otp_token=None):
        try:
            # print "auth",  AuthUser.objects.all() # only debug
            auth_user = AuthUser.objects.get(username=username)

            simple_login_valid = auth_user.check_password(password)

            if auth_user.otp_token:
                totp = TOTP(auth_user.otp_token)
                token_valid = totp.verify(otp_token)
            else:
                token_valid = True

            if simple_login_valid and token_valid:
                return auth_user
        except AuthUser.DoesNotExist:
            pass

        return None

    def get_user(self, user_id):
        try:
            return AuthUser.objects.get(pk=user_id)
        except AuthUser.DoesNotExist:
            return None
