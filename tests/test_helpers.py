# py.test tests/test_helpers.py --cov=lib.helpers --cov-report term-missing

from django.test import TestCase, RequestFactory
from lib.helpers import parse_http_range_header, HTTP_RANGE_DEFAULT


class HelpersTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def tearDown(self):
        pass

    def test_no_http_range(self):
        request = self.factory.get('/dummy')
        start, no = parse_http_range_header(request.META)

        assert (start, no) == HTTP_RANGE_DEFAULT

    def test_with_http_range_malformed(self):
        request = self.factory.get('/dummy', HTTP_RANGE='1-10')
        start, no = parse_http_range_header(request.META)

        assert (start, no) == HTTP_RANGE_DEFAULT

    def test_with_http_range_valid(self):
        request = self.factory.get('/dummy', HTTP_RANGE='Range: items=4-24')
        start, no = parse_http_range_header(request.META)

        assert (start, no) == (4, 24)