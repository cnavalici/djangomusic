# -*- coding: utf-8 -*-

# python manage.py test -v 2
# coverage run --source='lib/' manage.py test
# coverage report -m
# python manage.py test tests.test_dbmanager

# py.test tests/test_dbmanager.py --cov=lib.dbmanager --cov-report term-missing

import hashlib
import string
import random
from tempfile import NamedTemporaryFile

from unittest.mock import patch, MagicMock
from mock_django.query import QuerySetMock

from django.test import TestCase

from lib.dbmanager import DBManager
from lib.scanneditem import ScannedItem
from jukebox.models import ScannedItem as ScannedItemModel


class DBManagerTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_update_collection_empty(self):
        dbm = DBManager()
        result = dbm.update_collection([], False)

        assert result is None

    @patch('lib.dbmanager.ScannedItem')
    def test_post_pre_scan_preparations(self, mockedModel):
        dbm = DBManager()

        dbm.pre_scan_preparations()
        mockedModel.objects.update.assert_called_once()

        dbm.post_scan_preparations()
        mockedModel.not_scanned_objects.all.delete.assert_called_once()

        dbm.clear_all_db()
        mockedModel.objects.all.delete.assert_called_once()

    @patch('lib.dbmanager.ScannedItem')
    def test_update_collection_no_new_items(self, mockedModel):
        mock1 = self._create_scanned_item_model()
        mock2 = self._create_scanned_item_model()

        querySetMock = QuerySetMock(ScannedItemModel, mock1, mock2)
        mockedModel.objects = querySetMock

        dbm = DBManager()
        dbm.update_collection([mock1, mock2], False)

        querySetMock.assert_called_with([])

    @patch('lib.dbmanager.ScannedItem')
    def test_update_collection_new_items(self, mockedModel):
        mock1 = self._create_scanned_item_model()
        mock2 = self._create_scanned_item_model()

        mock3 = self._create_scanned_item()

        querySetMock = QuerySetMock(ScannedItemModel, mock1, mock2)
        mockedModel.objects = querySetMock

        dbm = DBManager()
        dbm.update_collection([mock1, mock2, mock3], False)

        querySetMock.assert_called_with([mock3.uid])

    @patch('lib.dbmanager.ScannedItem')
    def test_update_collection_removed_items(self, mockedModel):
        mock1 = self._create_scanned_item_model()
        mock2 = self._create_scanned_item_model()

        # import pdb; pdb.set_trace()
        mock1_changed = self._create_scanned_item()
        mock1_changed.get_db_object.return_value = mock1.uid
        mock1_changed.hashcontent = 'hascontent1-changed'
        mock1_changed.uid = mock1.uid

        querySetMock = QuerySetMock(ScannedItemModel, mock1, mock2)
        mockedModel.objects = querySetMock

        # import pdb; pdb.set_trace()
        dbm = DBManager()
        dbm.update_collection([mock1_changed, mock2], True)

        querySetMock.assert_called_with([mock1.uid])

    def _create_scanned_item_model(self):
        """helpers to create a mock for ScannedItemModel"""
        mock = MagicMock(spec=ScannedItemModel)

        random_filename = NamedTemporaryFile().name
        mock.uid = hashlib.md5(random_filename.encode('UTF-8')).hexdigest()

        random_hashcontent = "".join(random.choice(string.ascii_uppercase + string.digits) for _ in range(32))
        mock.hashcontent = random_hashcontent

        return mock

    def _create_scanned_item(self):
        """helpers to create a mock for ScannedItem"""
        mock = MagicMock(spec=ScannedItem)

        random_filename = NamedTemporaryFile().name
        mock.uid = hashlib.md5(random_filename.encode('UTF-8')).hexdigest()
        mock.get_db_object.return_value = mock.uid

        return mock