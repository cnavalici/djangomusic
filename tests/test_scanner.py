# -*- coding: utf-8 -*-

# python manage.py test -v 2
# coverage run --source='lib/' manage.py test
# coverage report -m

# py.test tests/test_scanner.py --cov=lib.scanner --cov-report term-missing

import pytest
import os

from fs.tempfs import TempFS

from django.test import TestCase
from lib.scanner import Scanner


class ScannerTestCase(TestCase):
    def setUp(self):
        self.ts = TempFS()
        parent = os.path.dirname(__file__)
        self.fixtures_path = os.path.join(parent, 'data')

    def tearDown(self):
        self.ts.close()

    def test_set_path_no_arguments(self):
        scanner = Scanner()
        self.assertRaises(TypeError, scanner.set_target_path)

    def test_set_path_empty(self):
        scanner = Scanner()
        self.assertRaises(Exception, scanner.set_target_path, '')

    def test_set_path_invalid(self):
        scanner = Scanner()
        self.assertRaises(Exception, scanner.set_target_path, '/dummy/path')

    def test_set_path_valid(self):
        tpath = self.create_dir('test')

        scanner = Scanner()
        scanner.set_target_path(tpath)

        assert tpath in scanner.paths

    def test_set_multiple_valid_paths(self):
        scanner = Scanner()

        tpath = [self.create_dir('test1'), self.create_dir('test2')]

        scanner.set_target_path(tpath)

        assert tpath == scanner.paths

    def test_clear_paths(self):
        scanner = Scanner()

        tpath = self.create_dir('test3')

        scanner.set_target_path(tpath)
        scanner.clear_target_paths()

        assert scanner.paths == []

    def test_set_callback_no_arg(self):
        with pytest.raises(TypeError):
            scanner = Scanner()
            scanner.set_callback()

    def test_set_callback_invalid_arg(self):
        with pytest.raises(Exception):
            scanner = Scanner()
            scanner.set_callback('')

    def test_set_callback_valid_arg(self):
        scanner = Scanner()

        def my_callback(): pass

        scanner.set_callback(my_callback)
        assert scanner.callback == my_callback

    def test_scan_no_paths_specified(self):
        scanner = Scanner()
        with pytest.raises(Exception):
            scanner.scan()

    # scan() tests START
    def test_scan_good_path_no_files(self):
        scanner = Scanner()
        scanner.set_target_path(self.fixtures_path)

        def my_callback(*args): pass
        scanner.set_callback(my_callback)

        scanner.scan()

        assert len(scanner.scanned_collection) == 2

    def test_scan_seal_broken(self):
        scanner = Scanner()
        scanner.set_target_path(self.fixtures_path)
        scanner.batch_size = 1

        def my_callback(*args): pass
        scanner.set_callback(my_callback)

        scanner.scan()
        assert scanner.seal_broken is True
    # scan() tests END

    def test_force_update(self):
        scanner = Scanner()
        scanner.set_force_update(True)
        assert scanner.force_update_flag is True

        scanner.set_force_update("something")
        assert scanner.force_update_flag is True

    def create_dir(self, dirname, recursive = False):
        self.ts.makedir(dirname, recursive)
        return self.ts.getsyspath(dirname)




