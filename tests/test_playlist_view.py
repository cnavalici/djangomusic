# -*- coding: utf-8 -*-

# python manage.py test -v 2
# coverage run --source='lib/' manage.py test
# coverage report -m
# python manage.py test tests.test_playlist_view

# py.test tests/test_playlist_view.py --cov=jukebox.views.playlist --cov-report term-missing

from django.test import TestCase
from django.core.urlresolvers import reverse

from jukeauth.models import AuthUser


class PlaylistViewTestCase(TestCase):
    fixtures = ['users.json']

    def setUp(self):
        super(PlaylistViewTestCase, self).setUp()
        self.client.login(username='admin', password='admin')

    def tearDown(self):
        super(PlaylistViewTestCase, self).tearDown()

    def test_create_playlist_wrong_http_verb(self):
        response = self.client.get(reverse('jukebox:playlist:create_playlist'), args=())

        assert response.status_code == 405

    def test_create_playlist_missing_name(self):
        response = self.client.post(reverse('jukebox:playlist:create_playlist'), {'playlistname': ''})

        assert response.status_code == 400
        assert 'error' in response.content.decode('utf-8')

    def test_create_playlist_good_data(self):
        response = self.client.post(reverse('jukebox:playlist:create_playlist'), {'playlistname': 'TestOne'})

        assert response.status_code == 200
        assert 'success' in response.content.decode('utf-8')

    def test_create_playlist_main_playlist(self):
        # make sure we have no default main playlist
        our_user = AuthUser.objects.get(pk=1)
        if our_user.main_playlist:
            our_user.main_playlist.delete()

        # creating a new playlist now it will populate also 'main_playlist' for that user
        self.client.post(reverse('jukebox:playlist:create_playlist'), {'playlistname': 'TestOne'})

        # post check of the result in the DB
        our_user = AuthUser.objects.get(pk=1)
        assert 'TestOne' == our_user.main_playlist.playlistname

    def test_get_playlist_main_identifier_default_values(self):
        response = self.client.get(reverse('jukebox:playlist:get_playlist', kwargs={'playlist_id': 'main'}))

        assert response.status_code == 200
        assert 'Content-Range' in response
        assert 'items 0-10/0' == response['Content-Range']

