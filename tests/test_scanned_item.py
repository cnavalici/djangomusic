# -*- coding: utf-8 -*-

# python manage.py test -v 2
# coverage run --source='lib/' manage.py test
# coverage report -m

import os.path
import hashlib
import pytest

from django.test import TestCase
from fs.osfs import OSFS

from lib.scanneditem import ScannedItem
from jukebox.models import ScannedItem as ScannedItemModel


class ScannedItemTestCase(TestCase):
    def setUp(self):
        parent = os.path.dirname(__file__)
        self.fs = OSFS(os.path.join(parent, 'data'))
        self.existing_mp3 = os.path.join(parent, 'data', 'Other Noises - S.L.A.S.H.', '01 - Other Noises - Slash.mp3')
        self.existing_flac = os.path.join(parent, 'data', 'Other Noises - S.L.A.S.H.', '01 - Other Noises - Slash ShortVersion.flac')

    def tearDown(self):
        pass

    def test_str_and_repr(self):
        si_mp3 = ScannedItem(self.existing_mp3)
        si_flac = ScannedItem(self.existing_flac)

        expected_str_mp3 = "Other Noises - Slash (S.L.A.S.H) [01 - Other Noises - Slash.mp3]"
        expected_str_flac = "Other Noises - Slash Short Version (S.L.A.S.H) [01 - Other Noises - Slash ShortVersion.flac]"

        assert si_mp3.__str__() == expected_str_mp3
        assert si_mp3.__repr__() == expected_str_mp3

        assert si_flac.__str__() == expected_str_flac
        assert si_flac.__repr__() == expected_str_flac

    def test_regular_item(self):
        si = ScannedItem(self.existing_mp3)
        db_obj = si.get_db_object()

        self.assertIsInstance(db_obj, ScannedItemModel)

    def test_item_non_existing_path(self):
        with pytest.raises(Exception):
            ScannedItem('/tmp/non_existing_path.mp3')

    def test_create_uid_within_scanneditem(self):
        si = ScannedItem(self.existing_mp3)
        expected = hashlib.md5(self.existing_mp3.encode('utf-8')).hexdigest()

        assert expected == si.uid

    def test_create_hash_content(self):
        si = ScannedItem(self.existing_mp3)

        si.album = "Fear of the dark"
        si.artist = "Iron Maiden"
        si.title = "From Here to Eternity"
        si.year = 1992

        expected = "831c32e7f9266a094181a4e4c13d4d53"

        assert expected == si.hashcontent
        assert 32 == len(si.hashcontent)

    def test_create_hash_album(self):
        si = ScannedItem(self.existing_mp3)

        si.album = "Fear of the dark"
        si.artist = "Iron Maiden"

        expected = "574931cddc8c15c7299eddc7cb799f0e"

        assert expected == si.hashalbum
        assert 32 == len(si.hashalbum)
