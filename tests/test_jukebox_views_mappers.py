# py.test tests/test_jukebox_views_mappers.py --cov=jukebox.views.mappers --cov-report term-missing

from django.test import TestCase
from mock_django.query import QuerySetMock
from unittest.mock import MagicMock
from lib.identitymanager import IdentityManager
from urllib.parse import quote

from jukebox.views.mappers import playlist_map, artists_map, albums_map, tracks_map
from jukebox.models import ScannedItem as ScannedItemModel


class JukeboxMappersTestCase(TestCase):
    def setUp(self):
        self.im = IdentityManager()

    def tearDown(self):
        pass

    def test_playlist_map_no_input_data(self):
        assert [] == playlist_map([])

    def test_playlist_map_input_data(self):
        mock1 = MagicMock(spec=ScannedItemModel)
        mock1.id = 1
        mock1.uid = 'f46a183f60954792ea12ee9349ea9f4b'
        mock1.title = 'Stick Around'
        mock1.album = 'Attempted Murder'
        mock1.artist = 'Steady Hussle'
        mock1.year = 2016
        mock1.genre = 'Christian Rock'

        mock2 = MagicMock(spec=ScannedItemModel)
        mock2.id = 2
        mock2.uid = 'e65a3c030adde8d5eaced99061166f7c'
        mock2.title = 'I want you'
        mock2.album = 'After Hours'
        mock2.artist = 'e65a3c030adde8d5eaced99061166f7c'
        mock2.year = 2015
        mock2.genre = 'Folk'

        querySetMock = QuerySetMock(ScannedItemModel, mock1, mock2)
        mapped = playlist_map(querySetMock)

        assert 2 == len(mapped)
        assert mock1.title == mapped[0]['title']
        assert mock2.year == mapped[1]['year']

        expected_keys = ['id', 'uid', 'title', 'album', 'artist', 'year', 'genre']
        self.assertCountEqual(expected_keys, list(mapped[0].keys()))

    def test_artists_map_no_input_data(self):
        assert {'id': 'root', 'children': []} == artists_map([])

    def test_artists_map_input_data(self):
        input_data = ['artist1', 'artist2', 'artist three']
        result = artists_map(input_data)
        children = result['children']

        assert 3 == len(children)

        expected_keys = ['id', 'name', 'children']
        self.assertCountEqual(expected_keys, list(children[0].keys()))

        # assertions about the content itself
        assert input_data[0] == children[0]['name']
        assert quote(self.im.identifier_artist + input_data[2]) == children[2]['id']
        assert children[1]['children']

    def test_albums_map_no_input_data(self):
        assert {'id': self.im.identifier_artist, 'children': []} == albums_map([], '')

    def test_albums_map_input_data(self):
        input_data = [
            {'album': '', 'hashalbum': 'ad58c3fee8fd68331f6ab7834c4ee42c', 'album_tracks_count': 2},
            {'album': 'Black Album', 'hashalbum': 'cfd4a33685e1c01a0cb2be83acdcb012', 'album_tracks_count': 6}
        ]
        result = albums_map(input_data, 'rootname')
        children = result['children']

        assert 2 == len(children)
        assert self.im.identifier_artist + 'rootname' == result['id']

        expected_keys = ['id', 'name', 'children']
        self.assertCountEqual(expected_keys, list(children[0].keys()))

        # assertions about the content itself
        assert quote(self.im.identifier_album + input_data[1]['hashalbum']) == children[1]['id']
        assert "- (02)" == children[0]['name']
        assert children[1]['children']

    def test_tracks_map_no_input_data(self):
        assert {'id': self.im.identifier_album, 'children': []} == tracks_map([], '')

    def test_tracks_map_input_data(self):
        input_data = [
            {'uid': 'e65a3c030adde8d5eaced99061166f7c', 'title': 'Stick around'},
            {'uid': 'f46a183f60954792ea12ee9349ea9f4b', 'title': 'I want you'},
            {'uid': 'c46a18aa609547e2ea12ee9349aa9f43', 'title': 'In the name...'}
        ]
        result = tracks_map(input_data, 'rootname')
        children = result['children']

        assert 3 == len(children)
        assert self.im.identifier_album + 'rootname' == result['id']

        # assertions about the content itself
        assert input_data[0]['title'] == children[0]['name']
        assert quote(self.im.identifier_track + input_data[1]['uid']) == children[1]['id']