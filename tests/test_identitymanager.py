# -*- coding: utf-8 -*-

# python manage.py test -v 2
# coverage run --source='lib/' manage.py test
# coverage report -m
# python manage.py test tests.test_identitymanager

from django.test import TestCase
from lib.identitymanager import IdentityManager

class IdentityManagerTestCase(TestCase):
    def setUp(self):
        self.identification = IdentityManager()

    def tearDown(self):
        pass

    def test_identification_invalid_data(self):
        self.assertFalse(self.identification.run_identification('invalid string'))

    def test_identification_artist(self):
        artist_name = 'Iron Maiden'
        artist_string = self.identification.identifier_artist + artist_name

        self.assertTrue(self.identification.run_identification(artist_string))

        self.assertEqual(self.identification.content, artist_name)

        self.assertTrue(self.identification.is_artist)
        self.assertFalse(self.identification.is_album)
        self.assertFalse(self.identification.is_track)

    def test_identification_album(self):
        album_name = 'Somewhere in time'
        album_string = self.identification.identifier_album + album_name

        self.assertTrue(self.identification.run_identification(album_string))

        self.assertEqual(self.identification.content, album_name)

        self.assertTrue(self.identification.is_album)
        self.assertFalse(self.identification.is_artist)
        self.assertFalse(self.identification.is_track)

    def test_identification_track(self):
        track_name = 'Wasted Years'
        track_string = self.identification.identifier_track + track_name

        self.assertTrue(self.identification.run_identification(track_string))

        self.assertEqual(self.identification.content, track_name)

        self.assertTrue(self.identification.is_track)
        self.assertFalse(self.identification.is_album)
        self.assertFalse(self.identification.is_artist)

    def test_reset_identification(self):
        self.identification.is_artist = True
        self.identification.is_track = True
        self.identification.is_album = True

        self.identification._IdentityManager__reset_identification()

        self.assertFalse(self.identification.is_artist)
        self.assertFalse(self.identification.is_track)
        self.assertFalse(self.identification.is_album)
