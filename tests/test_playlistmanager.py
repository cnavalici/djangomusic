# -*- coding: utf-8 -*-

# python manage.py test -v 2
# coverage run --source='lib/' manage.py test
# coverage report -m
# python manage.py test tests.test_playlistmanager

import pytest

from django.test import TestCase
from lib.playlistmanager import PlaylistManager
from jukeauth.models import AuthUser
from jukebox.models import Playlist, ScannedItem


class PlaylistManagerTestCase(TestCase):
    fixtures = ['users.json', 'scanneditems.json']

    def setUp(self):
        playlist = Playlist()
        playlist.owner = AuthUser.objects.get(pk=1)
        playlist.playlistname = 'Dummy Playlist'
        playlist.save()

        self.pm = PlaylistManager(playlist=playlist)
        self.playlist = playlist

        # from data-extra
        self.existent_artist = '2morrow2late'
        self.existent_album_hash = 'aecbba2058c5125f3f111c01e60098ef'
        self.existent_track_hash = 'f713ea8062f9b4739ec990592961e0f2'

    def tearDown(self):
        self.playlist.scanneditem_set = []
        self.playlist.save()

    def test_wrong_add_item(self):
        with pytest.raises(Exception):
            self.pm.add_item('WRONG_IDENTIFIER')

    def test_add_track_non_existent(self):
        with pytest.raises(Exception):
            non_existent_hash = 'XX0403e1bf44dc752e3646587234577b'
            self.pm.add_track(non_existent_hash)

    def test_add_track_existent(self):
        self.pm.add_track(self.existent_track_hash)
        self.assertEqual(self.playlist.scanneditem_set.count(), 1)

        # testing also the general (proxy) method
        trk_existent_hash = 'TRK_%s' % format(self.existent_track_hash)
        self.pm.add_item(trk_existent_hash)
        self.assertEqual(self.playlist.scanneditem_set.count(), 1)

    def test_add_album_non_existent(self):
        with pytest.raises(Exception):
            non_existent_hash = 'XXcbba2058c5125f3f111c01e60098ef'
            self.pm.add_album(non_existent_hash)

    def test_add_album_existent(self):
        self.pm.add_album(self.existent_album_hash)
        self.assertEqual(self.playlist.scanneditem_set.count(), 2)

        # testing also the general (proxy) method
        alb_existent_hash = "ALB_%s" % format(self.existent_album_hash)
        self.pm.add_item(alb_existent_hash)
        self.assertEqual(self.playlist.scanneditem_set.count(), 2)

    def test_add_artist_non_existent(self):
        with pytest.raises(Exception):
            non_existent_artist = 'ALB_This is not an artist oh yeah!'
            self.pm.add_track(non_existent_artist)

    def test_add_artist_no_tracks(self):
        # this is the case for orphaned artists (no tracks) - an exception
        ScannedItem.scanned_objects.filter(artist=self.existent_artist).delete()
        with pytest.raises(Exception):
            self.pm.add_artist(self.existent_artist)

    def test_add_artist_existent(self):
        self.pm.add_artist(self.existent_artist)
        self.assertEqual(self.playlist.scanneditem_set.count(), 2)

        # testing also the general (proxy) method
        art_existent_artist = "ART_%s" % format(self.existent_artist)
        self.pm.add_item(art_existent_artist)
        self.assertEqual(self.playlist.scanneditem_set.count(), 2)