# Playlist

## Considerations about Main Playlist

Every user should have one main playlist associated with the account. The id for this playlist is saved in _user_ object and the interface is recognize it by the keyword _main_.

At startup time, the application will ask for the main playlist content. Server side, this will be identified in the _logged user_ object and its relations retrieved. If there is no playlist at this moment, automatically a new one will be created and used as _main_ (empty content  by default).

Any existing playlist could become the _main_ one.

## API:

### Create a new playlist

    POST    /playlist/create/
    Params:
        playlistname = 'the name of the newly created playlist'

### Getting content for a playlist

    GET     /playlist/<playlist_id>/
    Params:
        playlist_id = 'main'|numeric
    
    If keyword 'main' is specified, then the item marked as 'main playlist' in the DB is used.
        
### Adding items to a playlist

    POST    /playlist/<playlist_id>/add/
    Params:
        playlist_id = 'main'|numeric
        item=ART_Some%20Artist
        item=ALB_aecbba2058c5125f3f111c01e60098ef
        item=TRK_250403e1bf35dc752e3646587234577b
        
    Response (json):
        {"success": "Item was added to the playlist."}
                            or
        {"error": <specific message>}

    Items could be artist (ART), album (ALB) or tracks (TRK).


### Last update:
10.06.2015      Cristian Năvălici
