# -*- coding: utf-8 -*-

from urllib.parse import unquote

class IdentityManager(object):
    """
    this class deals with items identification, as albums, tracks or artists
    """

    # elements identifiers
    identifier_artist = "ART_"
    identifier_album = "ALB_"
    identifier_track = "TRK_"

    def __init__(self):
        self.content = None
        self.is_track = False
        self.is_artist = False
        self.is_album = False

    def run_identification(self, element):
        element = unquote(element)
        self.__reset_identification()

        if element.startswith(self.identifier_album):
            self.is_album = True
            self.content = element[len(self.identifier_album):]

        if element.startswith(self.identifier_artist):
            self.is_artist = True
            self.content = element[len(self.identifier_artist):]

        if element.startswith(self.identifier_track):
            self.is_track = True
            self.content = element[len(self.identifier_track):]

        return bool(self.content)

    def __reset_identification(self):
        self.content = None
        self.is_track = False
        self.is_album = False
        self.is_artist = False
