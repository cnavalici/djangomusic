# -*- coding: utf-8 -*-
import os.path
import hashlib

from mutagen.easyid3 import EasyID3
from mutagen.flac import FLAC

from jukebox.models import ScannedItem as ScannedItemModel


class ScannedItem(object):
    """
    Structure:
    ---------------
    uid (hash based on fullpath)
    filename
    basename (seen as album folder)
    dirname (seen as the full path w/out album folder)
    title
    artist
    album
    year
    genre
    tracknumber
    hashcontent
    hashalbum (hash for artist + album)
    """

    def __init__(self, fullpath):
        if not os.path.exists(fullpath):
            raise Exception("Non existing path %s" % fullpath)

        # extract file common metadata
        path, self.filename = os.path.split(fullpath)

        self.basename = os.path.basename(path)
        self.dirname = os.path.dirname(path)

        extension = os.path.splitext(self.filename)[1].lower()

        # extract file specific data
        if extension == '.mp3':
            obj = EasyID3(fullpath)

        if extension == '.flac':
            obj = FLAC(fullpath)

        if obj:
            self._extract_info(obj)

        self.uid = self.create_unique_identifier(fullpath)
        self.hashcontent = self._create_hash_content()
        self.hashalbum = self._create_hash_album()

    def _extract_info(self, obj):
        self.year = obj['date'][0] if 'date' in obj else 0
        self.title = obj['title'][0] if 'title' in obj else ''
        self.artist = obj['artist'][0] if 'artist' in obj else ''
        self.genre = obj['genre'][0] if 'genre' in obj else ''
        self.album = obj['album'][0] if 'album' in obj else ''
        self.tracknumber = obj['tracknumber'][0] if 'tracknumber' in obj else 0

    def __str__(self):
        return "%s - %s (%s) [%s]" % (self.artist, self.title, self.album, self.filename)

    def __repr__(self):
        return "%s - %s (%s) [%s]" % (self.artist, self.title, self.album, self.filename)

    def get_db_object(self):
        db_obj = ScannedItemModel(
            uid=self.uid,
            filename=self.filename,
            basename=self.basename,
            dirname=self.dirname,
            title=self.title,
            artist=self.artist,
            album=self.album,
            year=self.year,
            genre=self.genre,
            tracknumber=self.tracknumber,
            hashcontent=self.hashcontent,
            hashalbum=self.hashalbum,
        )
        return db_obj

    def create_unique_identifier(self, fullpath):
        """
        this hash acts as an unique identifier in the db
        and is not related in any way to security - that's why is md5
        """
        return hashlib.md5(fullpath.encode('utf-8')).hexdigest()

    def _create_hash_content(self):
        """
        this hash acts as a control number for the content
        """
        source = "%s%s%s%s" % (self.album, self.artist, self.title, self.year)
        return hashlib.md5(source.encode('utf-8')).hexdigest()

    def _create_hash_album(self):
        """
        this hash acts as an unique identifier for album + artist
        """
        source = "%s%s" % (self.artist, self.album)
        return hashlib.md5(source.encode('utf-8')).hexdigest()
