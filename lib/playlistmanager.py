# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _

from jukebox.models import ScannedItem, Playlist
from .identitymanager import IdentityManager


class PlaylistManager(object):
    """
    related to operations around playlist
    """
    def __init__(self, playlist = None):
        self.playlist = playlist

    def add_item(self, item):
        """
        item might be ARTIST, TRACK or ALBUM
        """
        identity = IdentityManager()
        identified = identity.run_identification(item)

        if not identified:
            raise Exception("Unidentified element in request.")

        if identity.is_artist:
            self.add_artist(identity.content)

        if identity.is_album:
            self.add_album(identity.content)

        if identity.is_track:
            self.add_track(identity.content)

    def add_artist(self, item_data):
        """
        for an artist, item_data is a string (not a hash, e.g. 2morrow2late)
        """
        artist_tracks = ScannedItem.scanned_objects.filter(artist=item_data)
        if not artist_tracks.exists():
            raise Exception(_("Nothing to add for this artist."))

        # expand the list into arguments
        self.playlist.scanneditem_set.add(*artist_tracks)
        self.playlist.save()

    def add_album(self, item_data):
        """
        for an album, item_data is a hash (e.g. aecbba2058c5125f3f111c01e60098ef)
        """
        album_tracks = ScannedItem.scanned_objects.filter(hashalbum=item_data)
        if not album_tracks.exists():
            raise Exception(_("Album couldn't be found or is empty."))

        # expand the list into arguments
        self.playlist.scanneditem_set.add(*album_tracks)
        self.playlist.save()

    def add_track(self, item_data):
        """
        for a track, item_data is a hash (e.g. 250403e1bf35dc752e3646587234577b)
        """
        try:
            track = ScannedItem.scanned_objects.get(uid=item_data)
        except ScannedItem.DoesNotExist:
            raise Exception(_("Track couldn't be found."))

        self.playlist.scanneditem_set.add(track)
        self.playlist.save()

    def create_playlist(self, name, user):
        """
        creates a blank playlist for a specified user
        """
        new_playlist = Playlist.objects.create(playlistname=name, owner=user)

        return new_playlist