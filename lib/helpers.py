# -*- coding: utf-8 -*-

# default start and numbering values for http_range
HTTP_RANGE_DEFAULT = (0, 10)


def parse_http_range_header(request_meta):
    """
    :param request_meta: should include something as     X-Range items=0-24
    :return: integers start, no
    """
    # import pdb; pdb.set_trace() ## debug only
    # http_range = request_meta.GET.get('HTTP_RANGE', '')

    try:
        http_range = request_meta['HTTP_RANGE']
        try:
            start, no = http_range.split('=')[1].split('-')
        except IndexError:
            start, no = HTTP_RANGE_DEFAULT
    except KeyError:
        # default values here
        start, no = HTTP_RANGE_DEFAULT

    return int(start), int(no)
