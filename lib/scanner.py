# -*- coding: utf-8 -*-

import os
from .scanneditem import ScannedItem


class Scanner(object):
    extensions = ('mp3', 'flac')

    def __init__(self):
        self.paths = []
        self.scanned_collection = []
        self.batch_size = 10
        self.callback = None
        self.force_update_flag = False
        self.seal_broken = False # it will keep track if we have at least 1 callback

    def set_target_path(self, path):
        if not isinstance(path, list):
            path = [path, ]

        for p in path:
            if os.path.isdir(p):
                self.paths.append(p)
            else:
                raise Exception("Specified path is not a valid folder")

    def clear_target_paths(self):
        self.paths = []

    def scan(self):
        """
        main workhorse here; recursively iterating and calling an external function to process further
        the records (save to db, etc)
        :return:
        """
        if not self.paths:
            raise Exception("Specify first a path to follow")

        for single_path in self.paths:
            for dirpath, dirnames, files in os.walk(single_path):
                self._scanfunc(self.extensions, dirpath, files)
                # os.walk(single_path, self._scanfunc, self.extensions)

        # this last call will ensure that we don't have any scanned records left
        # for whose the batch size hasn't be reached
        self.callback(self.scanned_collection, self.force_update_flag)

    def _scanfunc(self, ext, dirname, names):
        for name in names:
            if name.lower().endswith(ext):
                si = ScannedItem(os.path.join(dirname, name))
                self._add_to_scanned_collection(si)

        # send what's left or not yet over batch size
        if self.seal_broken:
            self.callback(self.scanned_collection, self.force_update_flag)

    def set_callback(self, callback):
        if not callback:
            raise Exception("You should specify a valid callback")

        self.callback = callback

    def _add_to_scanned_collection(self, si):
        self.scanned_collection.append(si)

        if len(self.scanned_collection) == self.batch_size:
            self.callback(self.scanned_collection, self.force_update_flag)
            self.scanned_collection = []
            self.seal_broken = True

    def set_force_update(self, flag_value):
        self.force_update_flag = bool(flag_value)
