# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _

from .playlistmanager import PlaylistManager
from jukebox.models import Playlist

PLAYLIST_MAIN = 'main'


class UserPlaylistManager(object):
    """
    related to operations around an user and playlists
    """
    def __init__(self, user):
        self.user = user
        self.pm = PlaylistManager()

    def get_playlist(self, playlist_id):
        """
        Get the playlist identified by id
        :param playlist_id: integer or "main" for the default one
        :return:
        """
        if playlist_id == PLAYLIST_MAIN:
            playlist = self.user.main_playlist
            if not playlist:
                playlist = self.initialize_main_playlist()
        else:
            playlist = Playlist.objects.get(pk=playlist_id)

        return playlist

    def initialize_main_playlist(self):
        """
        Initialize the main playlist in case this is missing from user settings
        :return: playlist_id - newly created
        """
        new_playlist = self.pm.create_playlist(name="Main Playlist", user=self.user)

        self.user.main_playlist = new_playlist
        self.user.save()

        return new_playlist
