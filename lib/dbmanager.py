# -*- coding: utf-8 -*-
from django.core.exceptions import ObjectDoesNotExist
from jukebox.models import ScannedItem


class DBManager(object):
    def __init__(self):
        pass

    def update_collection(self, scanned_items, force_update):
        if not len(scanned_items):
            return

        scanned_uids = self._extract_uids(scanned_items)

        existing_items = ScannedItem.objects.filter(uid__in=scanned_uids)
        existing_items.update(scanflag=True)

        existing_uids = self._extract_uids(existing_items)
        new_uids = set(scanned_uids) - set(existing_uids)

        # Insert NEW items (on the disk but not in db)
        new_items = self._prepare_new_items(scanned_items, new_uids)
        ScannedItem.objects.bulk_create(new_items)

        # Do an update for TAGS changes if that's required
        if (force_update):
            self._do_update_on_existing(existing_items, scanned_items)

    def _extract_uids(self, collection_items):
        return [s.uid for s in collection_items if s]

    def _prepare_new_items(self, scanned_items, new_uids):
        return [scan.get_db_object() for scan in scanned_items if scan.uid in new_uids]

    def pre_scan_preparations(self):
        return ScannedItem.objects.update(scanflag=False)

    def post_scan_preparations(self):
        """ remove any orphan record from db - not on the disk anymore """
        return ScannedItem.not_scanned_objects.all().delete()

    def _do_update_on_existing(self, e_items, s_items):
        """ note that scanned items might be bigger than existing items but never less """
        mapped_s_items = {}
        for si in s_items:
            mapped_s_items[si.uid] = si

        modified_items_uids = []
        modified_items = []
        for ei in e_items:
            if ei.hashcontent != mapped_s_items[ei.uid].hashcontent:
                modified_items_uids.append(ei.uid)
                modified_items.append(mapped_s_items[ei.uid].get_db_object())

        if modified_items:
            ScannedItem.objects.filter(uid__in=modified_items_uids).delete()
            ScannedItem.objects.bulk_create(modified_items)

    def clear_all_db(self):
        return ScannedItem.objects.all().delete()

